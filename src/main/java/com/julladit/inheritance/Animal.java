/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.inheritance;

/**
 *
 * @author acer
 */
public class Animal {

    protected String name;
    protected int numberOflegs = 0;
    private String color;

    public Animal(String name, String color, int numberOflegs) {
        System.out.println("Animal Created");
        this.name = name;
        this.color = color;
        this.numberOflegs = numberOflegs;
    }

    public void walk() {
        System.out.println("Animal Walk");
    }

    public void speak() {
        System.out.println("Animal Speak");
        System.out.println("name: " + this.name + " color: " + this.color
                + " numberOflegs: " + this.numberOflegs);
    }

    public String getName() {
        return name;
    }

    public int getNumberOflegs() {
        return numberOflegs;
    }

    public String getColor() {
        return color;
    }

}
