/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.inheritance;

/**
 *
 * @author acer
 */
public class Duck extends Animal {

    private int numberOfwing;

    public Duck(String name, String color) {
        super(name, color, 2);
        System.out.println("Duck Created");
    }

    public void fly() {
        System.out.println("Duck: " + name + " Fly ");
    }

    @Override
    public void walk() {
        System.out.println("Duck: " + name + " Walk with " + numberOflegs + " legs. ");
    }

    @Override
    public void speak() {
        System.out.println("Duck: " + name + " speak > Quack Quack !!! ");
    }
}
