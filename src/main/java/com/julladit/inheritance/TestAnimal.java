/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.inheritance;

/**
 *
 * @author acer
 */
public class TestAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal("Evo", "White", 0);
        animal.speak();
        animal.walk();
        System.out.println("----------------");

        Dog Too = new Dog("Too", "Black&White");
        Too.speak();
        Too.walk();
        System.out.println("Too is Animal: " + (Too instanceof Animal));
        System.out.println("Too is Dog: " + (Too instanceof Dog));
        System.out.println("----------------");

        Dog Mome = new Dog("Mome", "White");
        Mome.speak();
        Mome.walk();
        System.out.println("Mome is Animal: " + (Mome instanceof Animal));
        System.out.println("Mome is Dog: " + (Mome instanceof Dog));
        System.out.println("----------------");

        Dog Bat = new Dog("Bat", "White");
        Bat.speak();
        Bat.walk();
        System.out.println("Bat is Animal: " + (Bat instanceof Animal));
        System.out.println("Bat is Dog: " + (Bat instanceof Dog));
        System.out.println("----------------");

        Dog Joe = new Dog("Joe", "Brown");
        Joe.speak();
        Joe.walk();
        System.out.println("Joe is Animal: " + (Joe instanceof Animal));
        System.out.println("Joe is Dog: " + (Joe instanceof Dog));
        System.out.println("----------------");

        Cat Kee = new Cat("Kee", "Orange");
        Kee.speak();
        Kee.walk();
        System.out.println("Kee is Animal: " + (Kee instanceof Animal));
        System.out.println("Kee is Dog: " + (Kee instanceof Cat));
        System.out.println("----------------");

        Duck Pao = new Duck("Pao", "Orange");
        Pao.speak();
        Pao.walk();
        Pao.fly();
        System.out.println("Pao is Animal: " + (Pao instanceof Animal));
        System.out.println("Pao is Dog: " + (Pao instanceof Duck));
        System.out.println("----------------");

        Duck Grab = new Duck("Grab", "Black");
        Grab.speak();
        Grab.walk();
        Grab.fly();
        System.out.println("Grab is Animal: " + (Grab instanceof Animal));
        System.out.println("Grab is Dog: " + (Grab instanceof Duck));
        System.out.println("----------------");
        System.out.println("");
        Animal[] animals = {Too, Mome, Bat, Joe, Kee, Pao, Grab};
        for(int i=0;i<animals.length;i++){
            animals[i].walk();
            animals[i].speak();
            System.out.println("-----------");
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
        }
    }
}
